// Dupli-DupliHood - Personal rework of ProfHankD's DupliHood: https://www.instructables.com/3D-Printed-Slide-Copy-Attachment/
// SPDX-License-Identifier: EUPL-1.2
/*
    Author:     flolilo
    License:    See SPDX-License-Identifier above.
    Repo:       https://codeberg.org/flolilo/Whetstone_Guides
    Version:    1.0
    Date:       2024-03-03
*/
/* Made/tested for/with:
    OpenSCAD    version 2021.01 & git e3e5f8e0c
    BOSL:       version 1.0.3
*/
include <BOSL/constants.scad>
use <BOSL/shapes.scad>

// Higher value makes font characters more detailed; tradeoff is higher render 
// times. 30 will do, 90 is more than plenty.
$fn = 60;

// Angle that you want to guide your knives at:
angle__degrees = 15;
// Size of the triangle is determined by the hypothenuse, which is the side 
// your knife will glide over:
hypothenuse__mm = 25;
// How wide the whetstone guide will be:
width__mm = 60;
// Nozzle size. This is only used for offsetting the font and isn't too 
// precise - 0.4 should work okay.
nozzle_size__mm = 0.4;

/* AUTOMATIC CALCULATIONS */
angle_alpha = angle__degrees;
length__mm = cos(angle_alpha) * hypothenuse__mm;
height__mm = sin(angle_alpha) * hypothenuse__mm;

echo(str("Guide has angle of ", angle__degrees, "° and is ", length__mm, " x ",
         width__mm__mm, " x ", height__mm, "mm LxWxH"));
difference(){
    right_triangle([length__mm, width__mm, height__mm]);
    translate([-0.8 * height__mm, 0, height__mm])
    rotate([0, 90, 0])
    right_triangle([height__mm, width__mm, height__mm]);
    translate([-0.8 * height__mm, -0.6 * height__mm, 0])
    rotate([60, angle__degrees, 0])
    cube([100, 100, 100]);
    translate([-0.8 * height__mm, width__mm + 0.6 * height__mm, 0])
    rotate([30, angle__degrees, 0])
    cube([100, 100, 100]);
    translate([length__mm - height__mm / 4, 0, -1])
    cube([height__mm, width__mm, height__mm]);
    translate([hypothenuse__mm / 2.2, width__mm / 2, height__mm / 2 - nozzle_size__mm])
    rotate([angle__degrees, 0, 90])
    linear_extrude(nozzle_size__mm * 1.5)
    text(str(angle__degrees, "°"), size=15, valign="center", halign="center", font="FiraCode Nerd Font Mono:style=SemiBold");
};

